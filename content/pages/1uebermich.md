title: Über mich
slug: uebermich
lang: de

Mein Name ist Markus Nölle und ich bin Professor für Nachrichtentechnik an der Hochschule für Technik und Wirtschaft ([HTW](www.htw-berlin.de)) in Berlin. 

Ich beschäftige mich vor allem mit Themen der optischen Nachrichtentechnik, des Mobilfunks und der digitalen Signalverarbeitung.

Nach mehreren Jahren der Forschung am [Fraunhofer Institut für Nachrichtentechnik](https://www.hhi.fraunhofer.de/index.html), dem Heinrich-Hertz-Institut, in Berlin war ich zwei Jahre am [Europäischen Patentamt](https://www.epo.org/de) in München als Patentprüfer für Patentanträge im Bereich des Mobilfunks (LTE, 5G) tätig.

