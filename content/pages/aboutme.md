title: About me
slug: uebermich
lang: en
translation: true

My name is Markus Nölle and I am professor of communications engineering at the Hochschule für Technik und Wirtschaft ([HTW](www.htw-berlin.de)) in Berlin. 

My research interests are optical and mobile communications, as well as digital signal processing techniques.

After some years of research at the [Fraunhofer Institut for Telecommunications](https://www.hhi.fraunhofer.de/en/index.html), Heinrich-Hertz-Institut in Berlin, I worked for two years as a patent examiner at the [European Patent Office](https://www.epo.org/en) in Munich, where I mainly worked on patent applications dealing with mobile communications (LTE, 5G). 