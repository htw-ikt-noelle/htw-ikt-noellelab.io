title: Lehre
slug: lehre
lang: de


Folgende Lehrveranstaltungen betreue ich im Studiengang Informations- und Kommunikationstechnik der HTW Berlin.

* **Signale und Systeme**

    In diesem Modul werden die Grundlagen zur Beschreibung zeitkontinuierlicher und zeitdiskreter Signale und Systeme sowohl im Zeit als auch im Frequenzbereich besprochen. Vornehmlich liegt das Augenmerk auf der Fourier-Analyse und linearen zeitinvarianten Systemen.

    Interaktive, quelloffene Lernmaterialien und Jupyter Notebooks zu diesem Kurs sind unter diesem [Gitlab repository](https://gitlab.com/htw-ikt-noelle/signaleundsysteme) zu finden.

* **Nachrichtenübertragung 1**

* **Nachrichtenübertragung 2**

* **Optische Nachrichtentechnik**

* **Digitale Breitbandkommunikation**

* 