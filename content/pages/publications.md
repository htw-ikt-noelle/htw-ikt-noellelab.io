title: Publications
slug: publications
lang: en
translation: true


Here you can find all publiations to which I contributed as (Co-)Autor. If you are interested in receiving a copy of one of these publications, please don't hesitate to contact me.


<span class="csl-left-margin">\[1\]
</span><span class="csl-right-inline">A. Johst, M. Nölle, L. Molle, N.
Perlot, M. Rohde, and R. Freund, “[<span class="nocase">Experimental
demonstration of robust spatial-diversity combining for coherent
free-space optical transmission
</span>](https://doi.org/10.1364/OFC.2024.W2A.31),” Proceedings of
optical fiber communication conference (OFC), 2024.</span>

<span class="csl-left-margin">\[2\]
</span><span class="csl-right-inline">A. Johst, L. Molle, N. Perlot, M.
Rothe, M. Rohde, and M. Nölle, “Data-aided multi-format DSP for
RobustFree-space coherent optical communication,” To appear in
proceedings of IEEE international conference on wireless for space and
extreme environments (WiSEE), 2024.</span>

<span class="csl-left-margin">\[3\]
</span><span class="csl-right-inline">A. Johst, N. Perlot, P. Hanne, M.
Rothe, A. Schreier, M. Nölle, and R. Freund,
“[<span class="nocase">Bidirectional real-time 10-Gbit/s free-space
optical link with a 4-aperture array under strong
scintillation</span>](https://doi.org/10.1109/ICSOS59710.2023.10491221),”
Proceedings of IEEE international conference on space optical systems
and applications (ICSOS), 2023.</span>

<span class="csl-left-margin">\[4\]
</span><span class="csl-right-inline">M. Hinrichs, B. Poddig, M. Nölle,
P. Hellwig, R. Freund, and V. Jungnickel,
“[<span class="nocase">Efficient Line Coding for Low-Power Optical
Wireless
Communications</span>](https://doi.org/10.1109/VTC2021-Fall52928.2021.9625557),”
2021 IEEE 94th vehicular technology conference (VTC2021-fall), pp.1–7,
2021.</span>

<span class="csl-left-margin">\[5\]
</span><span class="csl-right-inline">M. Nölle, M. S. Erkılınç, R.
Emmerich, C. Schmidt-Langhorst, R. Elschner, and C. Schubert,
“[<span class="nocase">Characterization and Linearization of High
Bandwidth Integrated Optical Transmitter
Modules</span>](https://doi.org/10.1109/ECOC48923.2020.9333184),”
Proceedings of european conference on optical communication (ECOC),
2020.</span>

<span class="csl-left-margin">\[6\]
</span><span class="csl-right-inline">R. Freund and M. Nölle,
“<span class="nocase">Optical and Microwave Technologies for
Telecommunication Networks</span>,” O. Strobel, ed. John Wiley, 2016,
pp.227–233.</span>

<span class="csl-left-margin">\[7\]
</span><span class="csl-right-inline">A. Napoli, M. Bohn, M. Nölle, J.
K. Fischer, and C. Schubert, “Elastic Optical Networks,” V. Lòpez and L.
Velasco, eds. Springer, 2016, pp.83–116.</span>

<span class="csl-left-margin">\[8\]
</span><span class="csl-right-inline">M. Nölle, C. Schubert, L. Molle,
R. Freund, A. Robinson, S. Desbruslais, and J. Schwartz,
“<span class="nocase">Application and Benefits of Raman-Enhanced
Amplification Schemes in Tomorrow’s Optical Submarine Systems</span>,”
SubOptic, 2016.</span>

<span class="csl-left-margin">\[9\]
</span><span class="csl-right-inline">P. Wilke Berenguer, M. Nölle, L.
Molle, T. Raman, A. Napoli, C. Schubert, and J. K. Fischer,
“<span class="nocase">Nonlinear digital pre-distortion of transmitter
components</span>,” *Journal of Lightwave Technology (invited
contribution)*, vol.34, no. 8, pp.1739–1745, 2016.</span>

<span class="csl-left-margin">\[10\]
</span><span class="csl-right-inline">S. Alreesh, C. Schmidt-Langhorst,
R. Elschner, F. Frey, P. Wilke Berenguer, L. Molle, M. Nölle, C.
Schubert, and J. K. Fischer, “<span class="nocase">Transmission of
2048SP-QAM Nyquist-WDM signals</span>,” ITG-fachtagung "photonische
netze", 2015.</span>

<span class="csl-left-margin">\[11\]
</span><span class="csl-right-inline">F. Da Ros, I. Sackey, M.
Jazayerifar, T. Richter, R. Elschner, C. Meuer, M. Nölle, L. Molle, K.
Petermann, J. K. Fischer, C. Schubert, and C. Peucheret,
“<span class="nocase">Optical phase conjugation for nonlinearity
compensation in WDM PDM 16-QAM transmission over dispersion-compensated
and dispersion-uncompensated links</span>,” Proceedings of IEEE
photonics society summer topical meeting on nonlinear-optical signal
processing (NOSP) (invited contribution), 2015.</span>

<span class="csl-left-margin">\[12\]
</span><span class="csl-right-inline">P. Layec, A. Dupas, M. Nölle, J.
K. Fischer, C. Schubert, J. M. Fabrega, M. S. Moreolo, N. Sambo, G.
Meloni, F. Fresi, A. Napoli, D. Rafique, M. Bohn, A. D’Errico, T.
Rahman, E. Hugues-Salas, Y. Yan, S. Yan, G. Zervas, and D. Simeonidou,
“<span class="nocase">IDEALIST data plane solutions for elastic optical
networks</span>,” In proceedings of european conference on networks and
communications (EuCNC), 2015.</span>

<span class="csl-left-margin">\[13\]
</span><span class="csl-right-inline">G. Meloni, T. Rahman, A. Napoli,
F. Fresi, N. Sambo, A. D’Errico, D. Rafique, M. Nölle, H. de Waardt, M.
Bohn, and L. Poti, “<span class="nocase">Experimental Comparison of
Transmission Performance for Nyquist WDM and Time Frequency
Time-Frequency Packing</span>,” *Journal of Lightwave Technology*,
vol.33, no. 24, pp.5261–5268, 2015.</span>

<span class="csl-left-margin">\[14\]
</span><span class="csl-right-inline">A. Napoli, M. Bohn, D. Rafique, A.
Stavdas, N. Sambo, L. Poti, M. Nölle, J. K. Fischer, E. Riccardi, A.
Pagano, A. D. Giglio, J. P. F. P. Gimenez, M. S. Moreolo, J. M. Fàbrega,
E. Hugues-Salas, G. Zervas, D. Simeounidou, P. Layec, A. D’Errico, and
T. Rahman, “<span class="nocase">Next Generation Elastic Optical
Networks: the Vision of the European Research Project IDEALIST</span>,”
*IEEE Communications Magazine*, vol.53, pp.152–162, 2015.</span>

<span class="csl-left-margin">\[15\]
</span><span class="csl-right-inline">M. Nölle, F. Frey, R. Elschner, C.
Schmidt-Langhorst, J. K. Fischer, and C. Schubert,
“<span class="nocase">Investigation of CAZAC Sequences for Data-Aided
Channel Estimation Considering Nonlinear Optical Transmission</span>,”
Proceedings of optical fiber communication conference (OFC),
2015.</span>

<span class="csl-left-margin">\[16\]
</span><span class="csl-right-inline">M. Nölle, F. Frey, R. Elschner, C.
Schmidt-Langhorst, J. K. Fischer, and C. Schubert,
“<span class="nocase">Performance of CAZAC Training Sequences in
Data-Aided Optical Transmission Systems</span>,” ITG-fachtagung
"photonische netze", 2015.</span>

<span class="csl-left-margin">\[17\]
</span><span class="csl-right-inline">T. Rahman, A. Napoli, G. Meloni,
F. Fresi, L. Poti, N. Sambo, D. Rafique, M. Nölle, C. Schubert, and M.
Bohn, “<span class="nocase">Experimental Comparison of 1.28 Tb/s Nyquist
WDM vs. Time-Frequency Packing</span>,” Proceedings of photonics in
switching, 2015.</span>

<span class="csl-left-margin">\[18\]
</span><span class="csl-right-inline">E. Riccardi, A. Pagano, E.
Hugues-Salas, G. Zervas, D. Simeonidou, A. D’Errico, M. Bohn, A. Napoli,
D. Rafique, M. Nölle, J. K. Fischer, N. Sambo, P. Castoldi, T. Rahman,
M. S. Moreolo, J. M. Fabrega, M. Gunkel, A. Lord, and J. Palacios,
“<span class="nocase">Sliceable Bandwidth Variable Transponders for
Elastic Optical Networks: The IDEALIST Vision</span>,” FOTONICA,
2015.</span>

<span class="csl-left-margin">\[19\]
</span><span class="csl-right-inline">I. Sackey, M. Nölle, T. Richter,
J. K. Fischer, M. Jazayerifar, K. Petermann, and C. Schubert,
“<span class="nocase">Evaluation of Kerr Nonlinearity Mitigation based
on Digital Backpropagation and Digital Coherent Superposition</span>,”
ITG-fachtagung "photonische netze", 2015.</span>

<span class="csl-left-margin">\[20\]
</span><span class="csl-right-inline">I. Sackey, M. Nölle, T. Richter,
J. K. Fischer, M. Jazayerifar, K. Petermann, and C. Schubert,
“<span class="nocase">Evaluation of Kerr Nonlinearity Mitigation based
on Digital Backpropagation and Digital Coherent Superposition</span>,”
Workshop der ITG fachgruppe 5.3.1, 2015.</span>

<span class="csl-left-margin">\[21\]
</span><span class="csl-right-inline">I. Sackey, T. Richter, M. Nölle,
M. Jazayerifar, K. Petermann, J. K. Fischer, and C. Schubert,
“<span class="nocase">Qualitative Comparison of Kerr Nonlinearity
Mitigation Schemes in a Dispersion-Managed Link for 4 x 28-GBd 16-QAM
Signals</span>,” *Journal of Lightwave Technology*, vol.33,
pp.4815–4825, 2015.</span>

<span class="csl-left-margin">\[22\]
</span><span class="csl-right-inline">N. Sambo, P. Castoldi, A.
D’Errico, E. Riccardi, A. Pagano, M. S. Moreolo, J. M. Fàbrega, D.
Rafique, A. Napoli, S. Frigerio, E. H. Salas, G. Zervas, M. Nölle, J. K.
Fischer, A. Lord, and J. P. F. P. Gimenez, “<span class="nocase">Next
Generation Sliceable Bandwidth Variable Transponder: the Vision of the
European Research Project IDEALIST</span>,” *IEEE Communications
Magazine*, vol.53, pp.163–171, 2015.</span>

<span class="csl-left-margin">\[23\]
</span><span class="csl-right-inline">P. Wilke Berenguer, T. Rahman, A.
Napoli, M. Nölle, J. K. Fischer, and C. Schubert,
“<span class="nocase">Nonlinear digital pre-distortion of transmitter
components</span>,” Proceedings of european conference on optical
communication (ECOC), 2015.</span>

<span class="csl-left-margin">\[24\]
</span><span class="csl-right-inline">J. K. Fischer, S. Alreesh, R.
Elschner, F. Frey, M. Nölle, C. Schmidt-Langhorst, and C. Schubert,
“<span class="nocase">Bandwidth-Variable Transceivers based on
Four-Dimensional Modulation Formats</span>,” *Journal of Lightwave
Technology (invited contribution)*, vol.32, pp.2886–2895, 2014.</span>

<span class="csl-left-margin">\[25\]
</span><span class="csl-right-inline">J. K. Fischer, C.
Schmidt-Langhorst, S. Alreesh, R. Elschner, F. Frey, P. Wilke Berenguer,
L. Molle, M. Nölle, and C. Schubert, “<span class="nocase">Transmission
of 512SP-QAM Nyquist-WDM signals</span>,” Proceedings of european
conference on optical communication (ECOC), 2014.</span>

<span class="csl-left-margin">\[26\]
</span><span class="csl-right-inline">J. K. Fischer, C.
Schmidt-Langhorst, S. Alreesh, R. Elschner, F. Frey, P. Wilke Berenguer,
L. Molle, M. Nölle, and C. Schubert, “<span class="nocase">Generation,
Transmission and Detection of 4D Set-Partitioning QAM Signals</span>,”
*Journal of Lightwave Technology (invited contribution)*, vol.33,
pp.1445–1451, 2014.</span>

<span class="csl-left-margin">\[27\]
</span><span class="csl-right-inline">A. Napoli, D. Rafique, M. Bohn, M.
Kuschnerov, B. Spinnler, and M. Nölle, “<span class="nocase">Performance
Dependence of Single-Carrier Digital Back-Propagation on Fiber Types and
Data Rates </span>,” Proceedings of optical fiber communication
conference (OFC), 2014.</span>

<span class="csl-left-margin">\[28\]
</span><span class="csl-right-inline">A. Napoli, M. Nölle, D. Rafique,
J. K. Fischer, B. Spinnler, T. Rahman, M. M. Mezghanni, and M. Bohn,
“<span class="nocase">On the next generation bandwidth variable
transponders for future flexible optical systems</span>,” In proceedings
of european conference on networks and communications (EuCNC),
2014.</span>

<span class="csl-left-margin">\[29\]
</span><span class="csl-right-inline">M. Nölle, F. Frey, R. Elschner, C.
Schmidt-Langhorst, A. Napoli, and C. Schubert,
“<span class="nocase">Performance Comparison of Different 8QAM
Constellations for the Use in Flexible Optical Networks</span>,”
Proceedings of optical fiber communication conference (OFC),
2014.</span>

<span class="csl-left-margin">\[30\]
</span><span class="csl-right-inline">T. Richter, M. Nölle, F. Frey, and
C. Schubert, “<span class="nocase">Generation and Coherent Reception of
107-GBd Optical Nyquist BPSK, QPSK and 16QAM</span>,” *IEEE Photonics
Technology Letters*, vol.26, pp.877–880, 2014.</span>

<span class="csl-left-margin">\[31\]
</span><span class="csl-right-inline">F. Da Ros, I. Sackey, R. Elschner,
T. Richter, C. Meuer, M. Nölle, M. Jazayerifar, K. Petermann, C.
Peucheret, and C. Schubert, “<span class="nocase">Kerr Nonlinearity
Compensation in a 5x28-GBd PDM 16-QAM WDM System Using Fiber-Based
Optical Phase Conjugation</span>,” Proceedings of european conference on
optical communication (ECOC), 2014.</span>

<span class="csl-left-margin">\[32\]
</span><span class="csl-right-inline">F. Da Ros, M. Nölle, C. Meuer, A.
Rahim, K. Voigt, A. Abboud, I. Sackey, S. Schwarz, L. Molle, G. Winzer,
L. Zimmermann, C. G. Schäffer, J. Bruns, K. Petermann, and C. Schubert,
“<span class="nocase">Experimental demonstration of an OFDM receiver
based on a silicon-nanophotonic discrete Fourier transform
filter</span>,” IEEE photonics conference, 2014.</span>

<span class="csl-left-margin">\[33\]
</span><span class="csl-right-inline">I. Sackey, R. Elschner, M. Nölle,
T. Richter, L. Molle, C. Meuer, M. Jazayerifar, S. Warm, K. Petermann,
and C. Schubert, “<span class="nocase">Characterization of a
Fiber-Optical Parametric Amplifier in a 5 x 28-GBd 16-QAM DWDM
System</span>,” Proceedings of optical fiber communication conference
(OFC), 2014.</span>

<span class="csl-left-margin">\[34\]
</span><span class="csl-right-inline">I. Sackey, F. Da Ros, M.
Jazayerifar, T. Richter, C. Meuer, M. Nölle, L. Molle, C. Peucheret, K.
Petermann, and C. Schubert, “<span class="nocase">Kerr nonlinearity
mitigation in 5x28-GBd PDM 16-QAM signal transmission over a
dispersion-uncompensated link with backward-pumped distributed Raman
amplification</span>,” *Optics Express*, vol.22, pp.27381–27391,
2014.</span>

<span class="csl-left-margin">\[35\]
</span><span class="csl-right-inline">M. Bohn, A. Napoli, A. D’Errico,
G. Ferraris, M. Gunkel, F. Jimenez, J. P. Fernandez-Palacios, P. Layec,
G. Zervas, N. Amaya, D. Simeonidou, J. K. Fischer, M. Nölle, C.
Schubert, N. Sambo, F. Cugini, P. Castoldi, M. S. Moreolo, J. M.
FAabrega, C. Okonkwo, and H. de Waardt, “<span class="nocase">Elastic
Optical Networks: the Vision of the ICT Project IDEALIST</span>,”
Proceedings of future network and mobile summit (FuNeMS), 2013.</span>

<span class="csl-left-margin">\[36\]
</span><span class="csl-right-inline">R. Elschner, F. Frey, C.
Schmidt-Langhorst, J. K. Fischer, M. Nölle, and C. Schubert,
“<span class="nocase">Software-defined Transponders for Future Flexible
Grid Networks</span>,” Proceedings of photonic networks and devices
(NETWORKS) (invited contribution), 2013.</span>

<span class="csl-left-margin">\[37\]
</span><span class="csl-right-inline">J. K. Fischer, S. Alreesh, R.
Elschner, F. Frey, M. Nölle, and C. Schubert,
“<span class="nocase">Bandwidth-Variable Transceivers Based on 4D
Modulation Formats for Future Flexible Networks</span>,” Proceedings of
european conference on optical communication (ECOC) (invited
contribution), 2013.</span>

<span class="csl-left-margin">\[38\]
</span><span class="csl-right-inline">L. Molle, M. Nölle, C. Schubert,
W. Wong, S. Webb, and J. Schwartz, “<span class="nocase">Single- versus
Dual-Carrier Transmission for Installed Submarine Cable
Upgrades</span>,” Proceedings of SubOptic, 2013.</span>

<span class="csl-left-margin">\[39\]
</span><span class="csl-right-inline">M. Nölle, C. Schubert, and R.
Freund, “<span class="nocase">Techniques to Realize Flexible Optical
Terabit per Second Transmission Systems</span>,” Proceedings of
photonics west (invited contribution), 2013.</span>

<span class="csl-left-margin">\[40\]
</span><span class="csl-right-inline">M. Nölle, “Spectrally Efficient
Optical Coherent Wavelength-Division Multiplexed Transmission Systems,”
PhD thesis, TU Berlin, 2013.</span>

<span class="csl-left-margin">\[41\]
</span><span class="csl-right-inline">C. Schmidt-Langhorst, F. Frey, M.
Nölle, R. Elschner, C. Meuer, P. Wilke Berenguer, and C. Schubert,
“<span class="nocase">Optimization of Subcarrier Spacing of 400 Gb/s
Dual-Carrier Nyquist PDM-16QAM in a Flexgrid Scenario</span>,”
Proceedings of european conference on optical communication (ECOC),
2013.</span>

<span class="csl-left-margin">\[42\]
</span><span class="csl-right-inline">S. Alreesh, J. Fischer, M. Nölle,
and C. Schubert, “<span class="nocase">Joint-Polarization Carrier Phase
Estimation for PS-QPSK Signals</span>,” *IEEE Photonics Technology
Letters*, vol.24, pp.1282–1284, 2012.</span>

<span class="csl-left-margin">\[43\]
</span><span class="csl-right-inline">J. K. Fischer, M. Nölle, L. Molle,
C. Schmidt-Langhorst, J. Hilt, R. Ludwig, and C. Schubert,
“<span class="nocase">Beyond 100G - High capacity transport technologies
for next generation optical core networks</span>,” Proceedings of future
network and mobile summit (FuNeMS), 2012.</span>

<span class="csl-left-margin">\[44\]
</span><span class="csl-right-inline">J. K. Fischer, M. Nölle, L. Molle,
C. Schmidt-Langhorst, and C. Schubert, “<span class="nocase">Is PS-QPSK
really an alternative to PDM-QPSK?</span>” ITG-fachtagung "photonische
netze", 2012.</span>

<span class="csl-left-margin">\[45\]
</span><span class="csl-right-inline">A. Hachmeister, M. Nölle, L.
Molle, R. Freund, and M. Rohde, “<span class="nocase">Performance
comparison of MSK and QPSK optical long haul DWDM transmission with
coherent detection</span>,” *Optics Express*, vol.20, pp.3877–3882,
2012.</span>

<span class="csl-left-margin">\[46\]
</span><span class="csl-right-inline">M. Nölle, J. K. Fischer, L. Molle,
C. Schmidt-Langhorst, and C. Schubert, “<span class="nocase">Influence
of Channel Spacing on Ultra Long-Haul WDM Transmission of 8x112 Gb/s
PS-QPSK Signals</span>,” Proceedings of optical fiber communication
conference (OFC), 2012.</span>

<span class="csl-left-margin">\[47\]
</span><span class="csl-right-inline">T. Richter, E. Palushani, C.
Schmidt-Langhorst, R. Ludwig, L. Molle, M. Nölle, and C. Schubert,
“<span class="nocase">Transmission of Single-Channel 16-QAM Data Signals
at Terabaud Symbol Rates</span>,” *Journal of Lightwave Technology*,
vol.30, pp.504–511, 2012.</span>

<span class="csl-left-margin">\[48\]
</span><span class="csl-right-inline">C. Schubert, J. K. Fischer, C.
Schmidt-Langhorst, R. Elschner, L. Molle, M. Nölle, and T. Richter,
“<span class="nocase">New Trends and Challenges in Optical Digital
Transmission Systems</span>,” Proceedings of photonics in switching
conference (invited contribution), 2012.</span>

<span class="csl-left-margin">\[49\]
</span><span class="csl-right-inline">C. Schubert, J. K. Fischer, C.
Schmidt-Langhorst, R. Elschner, L. Molle, M. Nölle, and T. Richter,
“<span class="nocase">New Trends and Challenges in Optical Digital
Transmission Systems</span>,” Proceedings of european conference on
optical communication (ECOC) (invited contribution), 2012.</span>

<span class="csl-left-margin">\[50\]
</span><span class="csl-right-inline">T. Tanimura, M. Nölle, J. K.
Fischer, and C. Schubert, “<span class="nocase">Analytical Results on
Back Propagation Nonlinear Compensator in Coherent Transmission
Systems</span>,” Proceedings of european conference on optical
communication (ECOC), 2012.</span>

<span class="csl-left-margin">\[51\]
</span><span class="csl-right-inline">T. Tanimura, M. Nölle, J. K.
Fischer, and C. Schubert, “<span class="nocase">Analytical results on
back propagation nonlinear compensator with coherent detection</span>,”
*Optics Express*, vol.20, pp.28779–28785, 2012.</span>

<span class="csl-left-margin">\[52\]
</span><span class="csl-right-inline">A. Beling, N. Ebel, A. Matiss, G.
Unterbörsch, M. Nölle, J. K. Fischer, J. Hilt, L. Molle, C. Schubert, F.
Verluise, and L. Fulop, “<span class="nocase">Fully-integrated
polarization-diversity coherent receiver module for 100G
DP-QPSK</span>,” Proceedings of optical fiber communication conference
(OFC), 2011.</span>

<span class="csl-left-margin">\[53\]
</span><span class="csl-right-inline">R. Elschner, T. Richter, M. Nölle,
J. Hilt, and C. Schubert, “<span class="nocase">Parametric amplification
of 28-GBd NRZ-16QAM signals</span>,” Proceedings of optical fiber
communication conf. And exposition (OFC), 2011.</span>

<span class="csl-left-margin">\[54\]
</span><span class="csl-right-inline">J. K. Fischer, L. Molle, M. Nölle,
C. Schmidt-Langhorst, J. Hilt, R. Ludwig, D. W. Peckham, and C.
Schubert, “[<span class="nocase">8 x 448-Gb/s WDM Transmission of 56-GBd
PDM 16-QAM OTDM Signals Over 250-km Ultralarge Effective Area
Fiber</span>](https://doi.org/.2098864),” *IEEE Photonics Technology
Letters*, vol.23, pp.239–241, 2011.</span>

<span class="csl-left-margin">\[55\]
</span><span class="csl-right-inline">J. K. Fischer, L. Molle, M. Nölle,
and D.-D. G. C. Schubert, “<span class="nocase">Experimental
Investigation of 28-GBd Polarization-Switched Quadrature Phase-Shift
Keying Signals</span>,” Proceedings of european conference on optical
communication (ECOC), 2011.</span>

<span class="csl-left-margin">\[56\]
</span><span class="csl-right-inline">J. K. Fischer, L. Molle, M. Nölle,
D.-D. G. C. Schmidt-Langhorst, and C. Schubert,
“<span class="nocase">Experimental Investigation of 84-Gb/s and 112-Gb/s
Polarization-Switched Quadrature Phase-Shift Keying Signals</span>,”
*Optics Express*, vol.19, pp.B667–B672, 2011.</span>

<span class="csl-left-margin">\[57\]
</span><span class="csl-right-inline">J. K. Fischer, R. Ludwig, L.
Molle, M. Nölle, A. Beling, C. C. Leonhardt, A. Matiss, A. Umbach, R.
Kunkel, H.-G. Bach, and C. Schubert, “<span class="nocase">Integrated
Coherent Receiver Modules for 100G Ethernet and Beyond</span>,” ITG
fachtagung "photonische netze", 2011.</span>

<span class="csl-left-margin">\[58\]
</span><span class="csl-right-inline">J. K. Fischer, M.Nölle, L. Molle,
C. Schmidt-Langhorst, and C. Schubert, “<span class="nocase">Nyquist-WDM
und PS-QPSK für zukünftige Tbit/s Systeme</span>,” Workshop der
ITG-fachgruppe 5.3.1, 2011.</span>

<span class="csl-left-margin">\[59\]
</span><span class="csl-right-inline">R. Freund, M. Nölle, M. Seimetz,
J. Hilt, J. Fischer, R. Ludwig, C. Schubert, H.-G. Bach, K.-O. Velthaus,
and M. Schell, “<span class="nocase">Higher-Order Modulation Formats for
Spectral-Efficient High-Speed Metro Systems</span>,” Proceedings of
photonics west (invited contribution), 2011.</span>

<span class="csl-left-margin">\[60\]
</span><span class="csl-right-inline">R. Ludwig, T. Richter, E.
Palushani, C. Schmidt-Langhorst, L. Molle, J. K. Fischer, M. Nölle, R.
Elschner, and C. Schubert, “<span class="nocase">Ultrafast transmission
systems using coherent technology</span>,” Opto-electronics and
communications conference (OECC), pp.808–811, 2011.</span>

<span class="csl-left-margin">\[61\]
</span><span class="csl-right-inline">A. Matiss, M. Nölle, J. K.
Fischer, C. C. Leonhardt, R. Ludwig, J. Hilt, L. Molle, C.
Schmidt-Langhorst, and C. Schubert,
“<span class="nocase">Characterization of an integrated coherent
receiver for 224 Gb/s polarization multiplexed 16-QAM
transmission</span>,” Proceedings of optical fiber communication
conference (OFC), 2011.</span>

<span class="csl-left-margin">\[62\]
</span><span class="csl-right-inline">M. Mussolin, D. Rafique, J.
Mårtensson, M. Forzati, J. K. Fischer, L. Molle, M. Nölle, C. Schubert,
and A. D. Ellis, “<span class="nocase">Polarization Multiplexed 224 Gb/s
16QAM Transmission Employing Digital Back-Propagation</span>,”
Proceedings of european conference on optical communication (ECOC),
2011.</span>

<span class="csl-left-margin">\[63\]
</span><span class="csl-right-inline">M. Nölle, J. K. Fischer, L. Molle,
C. Schmidt-Langhorst, D. Peckham, and C. Schubert,
“<span class="nocase">Comparison of 8x112 Gb/s PS-QPSK and PDM-QPSK
signals over transoceanic distances</span>,” *Optics Express*, vol.19,
pp.24370–24375, 2011.</span>

<span class="csl-left-margin">\[64\]
</span><span class="csl-right-inline">E. Palushani, C.
Schmidt-Langhorst, T. Richter, M. Nölle, R. Ludwig, and C. Schubert,
“<span class="nocase">Transmission of a Serial 5.1-Tb/s Data Signal
Using 16-QAM and Coherent Detection</span>,” Proceedings of european
conference on optical communication (ECOC), 2011.</span>

<span class="csl-left-margin">\[65\]
</span><span class="csl-right-inline">D. Rafique, M. Mussolin, J.
Mårtensson, M. Forzati, J. K. Fischer, L. Molle, M. Nölle, C. Schubert,
and A. D. Ellis, “<span class="nocase">Polarization multiplexed 16QAM
transmission employing modified digital back-propagation</span>,”
*Optics Express*, vol.19, pp.805–810, 2011.</span>

<span class="csl-left-margin">\[66\]
</span><span class="csl-right-inline">T. Richter, E. Palushani, C.
Schmidt-Langhorst, M. Nölle, R. Ludwig, J. K. Fischer, and C. Schubert,
“<span class="nocase">Single wavelength channel 10.2 Tb/s TDM-data
capacity using 16-QAM and coherent detection</span>,” Proceedings of
optical fiber communication conference (OFC) (postdeadline paper),
2011.</span>

<span class="csl-left-margin">\[67\]
</span><span class="csl-right-inline">M. Selmi, C. Gosset, M. Nölle, P.
Ciblat, and Y. Jaouen, “[<span class="nocase">Block-wise Digital Signal
Processing for PolMux QAM/PSK Optical Coherent
Systems</span>](https://doi.org/.2160251),” *Journal of Lightwave
Technology*, vol.29, pp.3070–3082, 2011.</span>

<span class="csl-left-margin">\[68\]
</span><span class="csl-right-inline">R. Freund, M. Nölle, C.
Schmidt-Langhorst, R. Ludwig, C. Schubert, G. Bosco, A. Carena, P.
Poggiolini, L. Oxenløwe, M. Galili, H. C. H. Mulvad, M. Winter, D.
Hillerkuss, R. Schmogrow, W. Freude, J. Leuthold, A. D. Ellis, F. C. G.
Gunning, J. Zhao, P. Frascella, S. K. Ibrahim, and N. M. Suibhne,
“<span class="nocase">Single- and multi-carrier techniques to build up
Tb/s per channel transmission systems</span>,” Proceedings of
international conference on transparent optical networks (ICTON)
(invited contribution), 2010.</span>

<span class="csl-left-margin">\[69\]
</span><span class="csl-right-inline">R. Freund, J. Hilt, M. Nölle, L.
Molle, M. Seimetz, J. K. Fischer, R. Ludwig, C. Schmidt-Langhorst, and
C. Schubert, “<span class="nocase">High Spectral Efficient Phase and
Quadrature Amplitude Modulation for Optical Fibre Transmission</span>,”
Proceedings of signal processing in photonic communications (SPPCom)
(invited contribution), 2010.</span>

<span class="csl-left-margin">\[70\]
</span><span class="csl-right-inline">J. Hilt, M. Nölle, L. Molle, M.
Seimetz, and R. Freund, “<span class="nocase">32 Gbaud real-time
FPGA-based multi-format transmitter for generation of higher-order
modulation formats</span>,” Proceedings of international conference on
optical internet (COIN), 2010.</span>

<span class="csl-left-margin">\[71\]
</span><span class="csl-right-inline">J. Hilt, M. Nölle, L. Molle, M.
Seimetz, and R. Freund, “<span class="nocase">32 Gbaud Real-Time
FPGA-Based Multi-Format Transmitter for Generation of Higher-Order
Modulation Formats</span>,” Workshop der ITG fachgruppe 5.3.1,
2010.</span>

<span class="csl-left-margin">\[72\]
</span><span class="csl-right-inline">M. Nölle, L. Molle, D. Gross, and
R. Freund, “<span class="nocase">Transmission of 5x62 Gbit/s DWDM
Coherent OFDM with a Spectral Efficiency of 7.2 Bit/s/Hz using Joint
64-QAM and 16-QAM Modulation</span>,” Proceedings of optical fiber
communication conference (OFC), 2010.</span>

<span class="csl-left-margin">\[73\]
</span><span class="csl-right-inline">M. Nölle, J. Hilt, L. Molle, M.
Seimetz, and R. Freund, “<span class="nocase">8x224 Gbit/s PDM 16QAM WDM
Transmission with Real-Time Signal Processing at the
Transmitter</span>,” Proceedings of european conference on optical
communication (ECOC), 2010.</span>

<span class="csl-left-margin">\[74\]
</span><span class="csl-right-inline">M. Nölle,
“<span class="nocase">Steigerung der Datenrate in optischen Systemen mit
hoher spektraler Effizienz</span>,” ITG-fachtagung "photonische netze",
2010.</span>

<span class="csl-left-margin">\[75\]
</span><span class="csl-right-inline">M. Seimetz, R. Freund, L. Molle,
M. Nölle, J. Hilt, and D.-D. Gross, “Spectrally Efficient Fiber
Transmission Using Higher-Order Modulation Formats,” FORCE inaugural
workshop, chalmers university of technology, gothenburg, sweden (invited
contribution), 2010.</span>

<span class="csl-left-margin">\[76\]
</span><span class="csl-right-inline">M. Nölle, M. Seimetz, and E.
Patzak, “<span class="nocase">System Performance of High-Order Optical
DPSK and Star QAM Modulation for Direct Detection Analyzed by
Semi-Analytical BER Estimation</span>,” *Journal of Lightwave
Technology*, vol.27, pp.4319–4329, 2009.</span>

<span class="csl-left-margin">\[77\]
</span><span class="csl-right-inline">M. Nölle and R. Freund,
“<span class="nocase">Comparison of 30 Gbit/s coherent 8PSK single
carrier and OFDM multi carrier systems with respect to transmission
reach</span>,” Proceedings of optical fiber communication conference
(OFC), 2009.</span>

<span class="csl-left-margin">\[78\]
</span><span class="csl-right-inline">M. Nölle, D. Gross, L. Molle, and
R. Freund, “<span class="nocase">WDM Performance of Spectral Efficient
Coherent OFDM</span>,” Workshop der ITG fachgruppe 5.3.1, 2009.</span>

<span class="csl-left-margin">\[79\]
</span><span class="csl-right-inline">M. Seimetz, M. Nölle, C. M.
Weinert, R. Freund, B. Spinnler, S. Spälter, and C. R. S. Fludger,
“<span class="nocase">Influence of neighbor channel modulation formats
on 112 Gbit/s and 42.8 Gbit/s WDM coherent polmux-QPSK transmission
systems</span>,” Proceedings of optical fiber communication conference
(OFC), 2009.</span>

<span class="csl-left-margin">\[80\]
</span><span class="csl-right-inline">M. Seimetz, M. Nölle, and E.
Patzak, “<span class="nocase">Optical Systems With High-Order DPSK and
Star QAM Modulation Based on Interferometric Direct Detection</span>,”
*Journal of Lightwave Technology*, vol.25, pp.1515–1530, 2007.</span>
