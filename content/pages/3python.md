title: Python
slug: python
lang: de

Ich entwickle zusammen mit anderen Personen ein Python-basiertes Paket zur Simulation von Übertragungssystemen und Signalverarbeitungsroutinen namens *scikit-comm*. Damit besteht auch die Möglichkeit Laborhardware anzusprechen und "Hardware in the Loop"-Experimente durchzuführen.

Den Code zu diesem Projekt finden Sie unter [Scikit-comm](https://gitlab.com/htw-ikt-noelle/scikit-comm), während die aktuelle Dokumentation auf [ReadtheDocs](https://scikit-comm.readthedocs.io/en/latest/) gehostet wird.