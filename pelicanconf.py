from datetime import datetime

AUTHOR = 'Markus Nölle'
SITENAME = 'Markus Nölle'
SITEURL = ""
SITETITLE = "Markus Nölle"
SITESUBTITLE = "Professor für Nachrichtentechnik"
SITEDESCRIPTION = ""
SITELOGO = SITEURL + "/images/noelle_markus.jpg"

ARTICLE_HIDE_TRANSLATION = False
BROWSER_COLOR = "#333"
ROBOTS = "index, follow"

COPYRIGHT_YEAR = datetime.now().year
COPYRIGHT_NAME = "Markus Nölle"

HOME_HIDE_TAGS = True
MAIN_MENU = True
DEFAULT_PAGINATION = 5

STATIC_PATHS = ["images"]
PATH = "content"
TIMEZONE = 'Europe/Berlin'
DEFAULT_LANG = 'de'
THEME = "./Flex"

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
        ("----",""),
        #("HTW Berlin", "https://htw-berlin.de"),
        #("Fachbereich 1", "https://www.f1.htw-berlin.de")
)

# Social widget
SOCIAL = (
    ("gitlab", "https://gitlab.com/htw-ikt-noelle"),
    ("linkedin", "https://de.linkedin.com/in/markus-n%C3%B6lle-724440137"),
    ("researchgate", "https://www.researchgate.net/profile/Markus-Noelle-3"),
    ("google", "https://scholar.google.de/citations?user=8hPYGaAAAAAJ&hl=de"),
    ("xing", "https://www.xing.com/profile/Markus_Noelle")
)

MENUITEMS = (
    ("Archives", "/archives.html"),
    ("Categories", "/categories.html"),
    ("Tags", "/tags.html"),
)

# Code style
PYGMENTS_STYLE = "default"#solarized-light"#"default"#"monokai"
THEME_COLOR_AUTO_DETECT_BROWSER_PREFERENCE = True
THEME_COLOR_ENABLE_USER_OVERRIDE = True

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True